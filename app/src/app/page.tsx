"use client";
import { Model } from "@/components/trangles";
import { Environment } from "@react-three/drei";
import { Canvas } from "@react-three/fiber";
import Image from "next/image";

export default function Home() {
  return (
    <main className="  h-screen">
      <Canvas className=" ">
        <Environment preset="forest" />
        <Model />
      </Canvas>
    </main>
  );
}
