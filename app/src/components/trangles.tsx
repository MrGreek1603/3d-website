import * as THREE from 'three'
import React, { useRef } from 'react'
import { Float, useGLTF } from '@react-three/drei'
import { GLTF } from 'three-stdlib'

type GLTFResult = GLTF & {
  nodes: {
    Plane: THREE.Mesh
    Plane001: THREE.Mesh
  }
  materials: {}
}

export function Model(props: JSX.IntrinsicElements['group']) {
  const { nodes, materials } = useGLTF('/gltf/triangle.gltf') as GLTFResult
  return (
    <group {...props} scale={2} position={[0, -2, 0]} rotation={[0,1,0]} dispose={null}>
   
     <Float floatIntensity={10}>

      <mesh
        castShadow
        receiveShadow
        geometry={nodes.Plane.geometry}
        material={nodes.Plane.material}
      />
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.Plane001.geometry}
        material={nodes.Plane001.material}
        position={[-0.453, 0, -0.945]}
        rotation={[Math.PI, 0, Math.PI]}
      />
     <ambientLight intensity={Math.PI / 2} />
    <spotLight position={[10, 10, 10]} angle={0.15} penumbra={1} decay={0} intensity={Math.PI} />
    <pointLight position={[-10, -10, -10]} decay={0} intensity={Math.PI} />
    </Float>
    </group>
  )
}

useGLTF.preload('/gltf/triangle.gltf')